﻿#include <iostream>
#include <iostream>
#include "Vector.h"

int main() {
	Vector<int> first;
	first.showVector();
	std::cout << first.lenVector() << std::endl;

	Vector<double> second(1.5, 3.5, 7.4);
	second.showVector();
	std::cout << second.lenVector() << std::endl;

	Vector<int> third(1, 2, 3);
	third.showVector();
	std::cout << third.lenVector() << std::endl;
}

