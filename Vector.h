#pragma once
#include <iostream>

template<typename T,
	typename = typename std::enable_if<std::is_arithmetic<T>::value, T>::type>
class Vector
{
private:
	T x, y, z;
public:
	Vector() {
		x = 0;
		y = 0;
		z = 0;
	}
	Vector(T x, T y, T z) {
		this->x = x;
		this->y = y;
		this->z = z;
	}
	void showX() {
		std::cout << "x = " << x << std::endl;
	}
	void showY() {
		std::cout << "y = " << y << std::endl;
	}
	void showZ() {
		std::cout << "z = " << z << std::endl;
	}
	void showVector() {
		std::cout << "x = " << x << "; y = " << y << "; z = " << z << std::endl;
	}
	T lenVector() {
		return std::sqrt(x * x + y * y + z * z);
	}
};
